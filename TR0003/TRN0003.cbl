      ******************************************************************
      * Author:WATCHARAPHON
      * Date:11/24/2022
      * Purpose:
      * Tectonics: cobc
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID. TRN0003.

       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT 100-INPUT-FILE ASSIGN
               TO "D:/Intern by NTT data/Test/TRN0003-1.txt"
               ORGANIZATION IS LINE SEQUENTIAL
               FILE STATUS IS WS-100-FILE-STATUS.
           SELECT 200-INPUT-FILE ASSIGN
               TO "D:/Intern by NTT data/Test/TRN0003-2.txt"
               ORGANIZATION IS LINE SEQUENTIAL
               FILE STATUS IS WS-200-FILE-STATUS.
           SELECT 300-INPUT-FILE ASSIGN
               TO "D:/Intern by NTT data/Test/TRN0003-3.txt"
               ORGANIZATION IS LINE SEQUENTIAL
               FILE STATUS IS WS-300-FILE-STATUS.
           SELECT 400-OUTPUT-FILE ASSIGN
               TO "D:/Intern by NTT data/Test/TRN0003-4.txt"
               ORGANIZATION IS LINE SEQUENTIAL
               FILE STATUS IS WS-400-FILE-STATUS.

       DATA DIVISION.
       FILE SECTION.
       FD 100-INPUT-FILE.
       01 100-INPUT-RECORD.
           05 100-PROFILE                  PIC X(16).
           05 FILLER                       PIC X.
           05 100-MERCHANT-NUMBER          PIC X(15).
           05 FILLER                       PIC X.
           05 100-MERCHANT-NAME            PIC X(14).
           05 FILLER                       PIC X.
           05 100-TAX-ID                   PIC X(13).
           05 FILLER                       PIC X.
           05 100-MCC                      PIC X(4).
           05 FILLER                       PIC X.
           05 100-MERCHANT-CONTACT         PIC X(16).
           05 FILLER                       PIC X.
           05 100-MERCHANT-ACCOUNT         PIC X(17).
       FD 200-INPUT-FILE.
       01 200-INPUT-RECORD.
           05 200-PROFILE                  PIC X(16).
           05 FILLER                       PIC X.
           05 200-MERCHANT-CONTACT         PIC X(16).
           05 FILLER                       PIC X.
           05 200-MERCHANT-ADDESS1         PIC X(18).
           05 FILLER                       PIC X.
           05 200-MERCHANT-ADDESS2         PIC X(18).
           05 FILLER                       PIC X.
           05 200-ZIP-CODE                 PIC X(8).
       FD 300-INPUT-FILE.
       01 300-INPUT-RECORD.
           05 300-PROFILE                  PIC X(16).
           05 FILLER                       PIC X.
           05 300-MERCHANT-ACCOUNT         PIC X(17).
           05 FILLER                       PIC X.
           05 300-MERCHANT-NUMBER          PIC X(15).
           05 FILLER                       PIC X.
       FD 400-OUTPUT-FILE.
       01 400-OUTPUT-RECORD.
           05 400-INDEX                    PIC X(4).
           05 400-MERCHANT-NUMBER          PIC X(20).
           05 400-MERCHANT-ADDESS          PIC X(40).
           05 400-MERCHANT-ACCOUNT         PIC X(20).

       WORKING-STORAGE SECTION.
       01  WS00293.
           05  WS-100-FILE-STATUS         PIC X(2)   VALUE SPACE.
               88  FILE-OK                           VALUE '00'.
               88  LENGTH-ERROR                      VALUE '04'.
               88  OPTIONAL-FILE-NOT-FOUND           VALUE '05'.
               88  INVALID-OPEN-CLOSE                VALUE '07'.
               88  FILE-AT-END                       VALUE '10'.
               88  SEQUENCE-ERROR                    VALUE '21'.
               88  NO-RECORD-FOUND                   VALUE '23'.
               88  NO-SPACE-AVAILABLE                VALUE '24' '34'.
               88  PERMANENT-IO-ERROR                VALUE '30'.
               88  FILE-NOT-FOUND                    VALUE '35'.
               88  INVALID-OPEN-MODE                 VALUE '37'.
               88  OPEN-ON-LOCKED-FILE               VALUE '38'.
               88  CONFLICTING-ATTRIBUTES            VALUE '39'.
               88  FILE-ALREADY-OPEN                 VALUE '41'.
               88  FILE-ALREADY-CLOSED               VALUE '42'.
               88  REWRITE-ERROR-NO-PREV-READ        VALUE '43'.
               88  DELETE-ERROR-NO-PREV-READ         VALUE '43'.
               88  WRITE-ERROR-RECORD-SIZE           VALUE '44'.
               88  SEQ-READ-AFTER-EOF                VALUE '46'.
               88  ERROR-NOT-OPEN-FOR-INPUT          VALUE '47'.
               88  ERROR-NOT-OPEN-FOR-OUTPUT         VALUE '48'.
               88  ERROR-NOT-OPEN-FOR-IO             VALUE '49'.
               88  QSAM-LOGIC-ERROR                  VALUE '90'.
               88  PASSWORD-FAILURE                  VALUE '91'.
               88  FILE-ACCESS-ERROR                 VALUE '92'.
               88  RESOURCE-NOT-AVAILABLE            VALUE '93'.
               88  FILE-INFO-MISSING                 VALUE '95'.
               88  MISSING-DD-CARD                   VALUE '96'.
               88  FILE-NOT-CLOSED                   VALUE '97'.
           05  WS-200-FILE-STATUS         PIC X(2)   VALUE SPACE.
               88  FILE-OK                           VALUE '00'.
               88  LENGTH-ERROR                      VALUE '04'.
               88  OPTIONAL-FILE-NOT-FOUND           VALUE '05'.
               88  INVALID-OPEN-CLOSE                VALUE '07'.
               88  FILE-AT-END                       VALUE '10'.
               88  SEQUENCE-ERROR                    VALUE '21'.
               88  NO-RECORD-FOUND                   VALUE '23'.
               88  NO-SPACE-AVAILABLE                VALUE '24' '34'.
               88  PERMANENT-IO-ERROR                VALUE '30'.
               88  FILE-NOT-FOUND                    VALUE '35'.
               88  INVALID-OPEN-MODE                 VALUE '37'.
               88  OPEN-ON-LOCKED-FILE               VALUE '38'.
               88  CONFLICTING-ATTRIBUTES            VALUE '39'.
               88  FILE-ALREADY-OPEN                 VALUE '41'.
               88  FILE-ALREADY-CLOSED               VALUE '42'.
               88  REWRITE-ERROR-NO-PREV-READ        VALUE '43'.
               88  DELETE-ERROR-NO-PREV-READ         VALUE '43'.
               88  WRITE-ERROR-RECORD-SIZE           VALUE '44'.
               88  SEQ-READ-AFTER-EOF                VALUE '46'.
               88  ERROR-NOT-OPEN-FOR-INPUT          VALUE '47'.
               88  ERROR-NOT-OPEN-FOR-OUTPUT         VALUE '48'.
               88  ERROR-NOT-OPEN-FOR-IO             VALUE '49'.
               88  QSAM-LOGIC-ERROR                  VALUE '90'.
               88  PASSWORD-FAILURE                  VALUE '91'.
               88  FILE-ACCESS-ERROR                 VALUE '92'.
               88  RESOURCE-NOT-AVAILABLE            VALUE '93'.
               88  FILE-INFO-MISSING                 VALUE '95'.
               88  MISSING-DD-CARD                   VALUE '96'.
               88  FILE-NOT-CLOSED                   VALUE '97'.
           05  WS-300-FILE-STATUS         PIC X(2)   VALUE SPACE.
               88  FILE-OK                           VALUE '00'.
               88  LENGTH-ERROR                      VALUE '04'.
               88  OPTIONAL-FILE-NOT-FOUND           VALUE '05'.
               88  INVALID-OPEN-CLOSE                VALUE '07'.
               88  FILE-AT-END                       VALUE '10'.
               88  SEQUENCE-ERROR                    VALUE '21'.
               88  NO-RECORD-FOUND                   VALUE '23'.
               88  NO-SPACE-AVAILABLE                VALUE '24' '34'.
               88  PERMANENT-IO-ERROR                VALUE '30'.
               88  FILE-NOT-FOUND                    VALUE '35'.
               88  INVALID-OPEN-MODE                 VALUE '37'.
               88  OPEN-ON-LOCKED-FILE               VALUE '38'.
               88  CONFLICTING-ATTRIBUTES            VALUE '39'.
               88  FILE-ALREADY-OPEN                 VALUE '41'.
               88  FILE-ALREADY-CLOSED               VALUE '42'.
               88  REWRITE-ERROR-NO-PREV-READ        VALUE '43'.
               88  DELETE-ERROR-NO-PREV-READ         VALUE '43'.
               88  WRITE-ERROR-RECORD-SIZE           VALUE '44'.
               88  SEQ-READ-AFTER-EOF                VALUE '46'.
               88  ERROR-NOT-OPEN-FOR-INPUT          VALUE '47'.
               88  ERROR-NOT-OPEN-FOR-OUTPUT         VALUE '48'.
               88  ERROR-NOT-OPEN-FOR-IO             VALUE '49'.
               88  QSAM-LOGIC-ERROR                  VALUE '90'.
               88  PASSWORD-FAILURE                  VALUE '91'.
               88  FILE-ACCESS-ERROR                 VALUE '92'.
               88  RESOURCE-NOT-AVAILABLE            VALUE '93'.
               88  FILE-INFO-MISSING                 VALUE '95'.
               88  MISSING-DD-CARD                   VALUE '96'.
               88  FILE-NOT-CLOSED                   VALUE '97'.
           05  WS-400-FILE-STATUS         PIC X(2)   VALUE SPACE.
               88  FILE-OK                           VALUE '00'.
               88  LENGTH-ERROR                      VALUE '04'.
               88  OPTIONAL-FILE-NOT-FOUND           VALUE '05'.
               88  INVALID-OPEN-CLOSE                VALUE '07'.
               88  FILE-AT-END                       VALUE '10'.
               88  SEQUENCE-ERROR                    VALUE '21'.
               88  NO-RECORD-FOUND                   VALUE '23'.
               88  NO-SPACE-AVAILABLE                VALUE '24' '34'.
               88  PERMANENT-IO-ERROR                VALUE '30'.
               88  FILE-NOT-FOUND                    VALUE '35'.
               88  INVALID-OPEN-MODE                 VALUE '37'.
               88  OPEN-ON-LOCKED-FILE               VALUE '38'.
               88  CONFLICTING-ATTRIBUTES            VALUE '39'.
               88  FILE-ALREADY-OPEN                 VALUE '41'.
               88  FILE-ALREADY-CLOSED               VALUE '42'.
               88  REWRITE-ERROR-NO-PREV-READ        VALUE '43'.
               88  DELETE-ERROR-NO-PREV-READ         VALUE '43'.
               88  WRITE-ERROR-RECORD-SIZE           VALUE '44'.
               88  SEQ-READ-AFTER-EOF                VALUE '46'.
               88  ERROR-NOT-OPEN-FOR-INPUT          VALUE '47'.
               88  ERROR-NOT-OPEN-FOR-OUTPUT         VALUE '48'.
               88  ERROR-NOT-OPEN-FOR-IO             VALUE '49'.
               88  QSAM-LOGIC-ERROR                  VALUE '90'.
               88  PASSWORD-FAILURE                  VALUE '91'.
               88  FILE-ACCESS-ERROR                 VALUE '92'.
               88  RESOURCE-NOT-AVAILABLE            VALUE '93'.
               88  FILE-INFO-MISSING                 VALUE '95'.
               88  MISSING-DD-CARD                   VALUE '96'.
               88  FILE-NOT-CLOSED                   VALUE '97'.

       01 WS-TIMESTAMP.
           05 WS-DATE-NOW                  PIC X(12).
           05 WS-TIME-NOW                  PIC X(12).

       01 WS-INPUT-RECORD1.
           05 WS-PROFILE                   PIC X(16).
           05 FILLER                       PIC X.
           05 WS-MERCHANT-NUMBER           PIC X(15).
           05 FILLER                       PIC X.
           05 WS-MERCHANT-NAME             PIC X(14).
           05 FILLER                       PIC X.
           05 WS-TAX-ID                    PIC X(13).
           05 FILLER                       PIC X.
           05 WS-MCC                       PIC X(4).
           05 FILLER                       PIC X.
           05 WS-MERCHANT-CONTACT          PIC X(16).
           05 FILLER                       PIC X.
           05 WS-MERCHANT-ACCOUNT          PIC X(17).

       01 WS-INPUT-RECORD2.
           05 WS-PROFILE                   PIC X(16).
           05 FILLER                       PIC X.
           05 WS-MERCHANT-CONTACT          PIC X(16).
           05 FILLER                       PIC X.
           05 WS-MERCHANT-ADDESS1          PIC X(18).
           05 FILLER                       PIC X.
           05 WS-MERCHANT-ADDESS2          PIC X(18).
           05 FILLER                       PIC X.
           05 WS-ZIP-CODE                  PIC X(8).

       01 WS-INPUT-RECORD3.
           05 WS-PROFILE                   PIC X(16).
           05 FILLER                       PIC X.
           05 WS-MERCHANT-ACCOUNT          PIC X(17).
           05 FILLER                       PIC X.
           05 WS-MERCHANT-NUMBER           PIC X(15).

       01 WS-OUTPUT-RECORD4.
           05 WS-INDEX                     PIC 9(6).
           05 FILLER                       PIC XX.
           05 WS-OUT-MERCHANT-NUMBER       PIC X(17).
           05 WS-OUT-MERCHANT-ADDESS1      PIC X(11).
           05 WS-OUT-MERCHANT-ADDESS2      PIC X(10).
           05 WS-OUT-ZIP-CODE              PIC X(7).
           05 WS-OUT-MERCHANT-ACCOUNT      PIC X(20).

       01 WS-TOTAL-LINE.
           05 WS-FILLER                    PIC X(8)  VALUE "TOTAL = ".
           05 WS-OUT-REC-CNT               PIC Z9.

       01 WS-CALCULATION.
           05 WS-RECORD-COUNT.
               10 WS-100-REC-COUNT         PIC S9(5) VALUE ZERO.
               10 WS-200-REC-COUNT         PIC S9(5) VALUE ZERO.
               10 WS-300-REC-COUNT         PIC S9(5) VALUE ZERO.
               10 WS-400-REC-COUNT         PIC S9(5) VALUE ZERO.
       01 WS-ARRAY.
           05 WS-ARRAY-FILE2 OCCURS 10 INDEXED BY WS-IDX.
               10 PROFILE                  PIC X(16).
               10 FILLER                   PIC X.
               10 MERCHANT-CONTACT         PIC X(16).
               10 FILLER                   PIC X.
               10 MERCHANT-ADDESS1         PIC X(18).
               10 FILLER                   PIC X.
               10 MERCHANT-ADDESS2         PIC X(18).
               10 FILLER                   PIC X.
               10 ZIP-CODE                 PIC X(8).

           05 WS-ARRAY-FILE3               OCCURS 10 TIMES
                                           INDEXED BY WS-IDXC.
               10 PROFILE                  PIC X(16).
               10 FILLER                   PIC X.
               10 MERCHANT-ACCOUNT         PIC X(17).
               10 FILLER                   PIC X.
               10 MERCHANT-NUMBER          PIC X(15).
               10 FILLER                   PIC X.
       01 WS-REPORT-FORMAT.
           05 WS-HEADER-1-LINE.
               10 FILLER                   PIC X(18)
                                           VALUE "Report-no: TRR0003".
               10 FILLER                   PIC X(15)
                                           VALUE SPACES.
               10 WS-INCOME-REPORT         PIC X(18)
                                           VALUE "Income Report".
               10 FILLER                   PIC X(10)
                                           VALUE SPACES.
               10 WS-DATE1                 PIC X(6)
                                           VALUE "Date: ".
               10 WS-DAY                   PIC X(2).
               10 FILLER                   PIC X(1) VALUE "/".
               10 WS-MTH                   PIC X(2).
               10 WS-FILLER                PIC X(1) VALUE "/".
               10 WS-YEARS                 PIC X(2).
           05 WS-HEADER-2-LINE.
                10 FILLER                  PIC X(18)
                                           VALUE "Program  : TRN0003".
                10 FILLER                  PIC X(43)
                                           VALUE SPACES.
                10 WS-TIME                 PIC X(6)
                                           VALUE "Time: ".
                10 WS-HOURS                PIC X(2).
                10 FILLER                  PIC X(1)  VALUE ":".
                10 WS-MIN                  PIC X(2).
                10 FILLER                  PIC X(1)  VALUE ":".
                10 WS-SEC                  PIC X(2).
           05 WS-ENTER-LINE             PIC X     VALUE SPACE.
           05 WS-HEADER-4-LINE.
                10 FILLER                  PIC X(8)  VALUE "Seq.".
                10 FILLER                  PIC X(17)
                                           VALUE "Merchant Number".
                10 FILLER                  PIC X(28)
                                           VALUE "Merchant Address".
                10 FILLER                  PIC X(20)
                                           VALUE "Merchant Account".

       01 WS-SUB                           PIC 9(2).
       01 WS-LINE-COUNT                    PIC S9(3) VALUE +55.
       01 WS-LINE-MAX                      PIC S9(3) VALUE +55.
       01 WS-LINE-HEADER                   PIC S9(3) VALUE +5.
       01 WS-ARRAY-MAX                     PIC S9(3) VALUE +10.

       PROCEDURE DIVISION.
       0000-MAIN.
           PERFORM 1000-INITAIL            THRU 1000-EXIT
           PERFORM 2000-PROCESS            THRU 2000-EXIT
               UNTIL FILE-AT-END OF WS-100-FILE-STATUS
           PERFORM 3000-END                THRU 3000-EXIT

           GOBACK
       .
       1000-INITAIL.
           ACCEPT WS-DATE-NOW FROM DATE
           ACCEPT WS-TIME-NOW FROM TIME

           PERFORM 1100-FORMAT-DATETIME
           OPEN INPUT  100-INPUT-FILE
                       200-INPUT-FILE
                       300-INPUT-FILE
                OUTPUT 400-OUTPUT-FILE
           PERFORM 10000-CHECK-FILE

           PERFORM 8000-READ-FILE1         THRU 8000-EXIT
           PERFORM 4000-LOAD-ARRAY         THRU 4000-EXIT
       .
       1100-FORMAT-DATETIME.
           MOVE WS-DATE-NOW(1:2)           TO WS-DAY
           MOVE WS-DATE-NOW(3:2)           TO WS-MTH
           MOVE WS-DATE-NOW(5:2)           TO WS-YEARS

           MOVE WS-TIME-NOW(1:2)           TO WS-HOURS
           MOVE WS-TIME-NOW(3:2)           TO WS-MIN
           MOVE WS-TIME-NOW(5:2)           TO WS-SEC
           .
       1000-EXIT.
           EXIT.
       2000-PROCESS.
           PERFORM 5000-CHK-HEADER         THRU 5000-EXIT
           PERFORM 6000-MOVE-OUTPUT        THRU 6000-EXIT
           PERFORM 8000-READ-FILE1         THRU 8000-EXIT
       .
       2000-EXIT.
           EXIT.
       3000-END.
           MOVE WS-100-REC-COUNT           TO WS-OUT-REC-CNT
           MOVE WS-ENTER-LINE           TO 400-OUTPUT-RECORD
           PERFORM 7000-WRITE-FILE4        THRU 7000-EXIT
           MOVE WS-TOTAL-LINE              TO 400-OUTPUT-RECORD
           PERFORM 7000-WRITE-FILE4        THRU 7000-EXIT

           DISPLAY "FILE 1 = " WS-100-REC-COUNT " RECORD."
           DISPLAY "FILE 2 = " WS-200-REC-COUNT " RECORD."
           DISPLAY "FILE 3 = " WS-300-REC-COUNT " RECORD."
           DISPLAY "FILE 4 = " WS-400-REC-COUNT " RECORD."

           CLOSE 100-INPUT-FILE
                 200-INPUT-FILE
                 300-INPUT-FILE
                 400-OUTPUT-FILE
           PERFORM 10000-CHECK-FILE
        .
       3000-EXIT.
           EXIT.
       4000-LOAD-ARRAY.
           MOVE ZERO                       TO WS-SUB
           PERFORM 8100-READ-FILE2         THRU 8100-EXIT
           PERFORM UNTIL FILE-AT-END       OF WS-200-FILE-STATUS
                                           OR WS-SUB >= WS-ARRAY-MAX
               ADD +1                      TO WS-SUB
               MOVE 200-INPUT-RECORD       TO WS-ARRAY-FILE2(WS-SUB)
               PERFORM 8100-READ-FILE2     THRU 8100-EXIT
           END-PERFORM
           IF WS-SUB >= WS-ARRAY-MAX AND FILE-OK OF WS-200-FILE-STATUS
              DISPLAY "ABEND MESSAGE"
              PERFORM 9000-ABEND           THRU 9000-EXIT
           END-IF

           MOVE ZERO                       TO WS-SUB
           PERFORM 8200-READ-FILE3         THRU 8200-EXIT
           PERFORM UNTIL FILE-AT-END       OF WS-300-FILE-STATUS
                                           OR WS-SUB >= WS-ARRAY-MAX
               ADD +1                      TO WS-SUB
               MOVE 300-INPUT-RECORD       TO WS-ARRAY-FILE3(WS-SUB)
               PERFORM 8200-READ-FILE3     THRU 8200-EXIT
           END-PERFORM
           IF WS-SUB >= WS-ARRAY-MAX   AND FILE-OK OF WS-300-FILE-STATUS
              DISPLAY "ABEND MESSAGE"
              PERFORM 9000-ABEND           THRU 9000-EXIT
           END-IF
       .
       4000-EXIT.
           EXIT.
       5000-CHK-HEADER.
           ADD +1                          TO WS-LINE-COUNT
           IF WS-LINE-COUNT >= WS-LINE-MAX
               MOVE WS-HEADER-1-LINE       TO 400-OUTPUT-RECORD
               PERFORM 7000-WRITE-FILE4    THRU 7000-EXIT
               MOVE WS-HEADER-2-LINE       TO 400-OUTPUT-RECORD
               PERFORM 7000-WRITE-FILE4    THRU 7000-EXIT
               MOVE WS-ENTER-LINE       TO 400-OUTPUT-RECORD
               PERFORM 7000-WRITE-FILE4    THRU 7000-EXIT

               MOVE WS-HEADER-4-LINE       TO 400-OUTPUT-RECORD
               PERFORM 7000-WRITE-FILE4    THRU 7000-EXIT
               MOVE WS-LINE-HEADER         TO WS-LINE-COUNT
           END-IF
       .
       5000-EXIT.
           EXIT.
       6000-MOVE-OUTPUT.
           MOVE 100-INPUT-RECORD TO WS-INPUT-RECORD1
           SEARCH WS-ARRAY-FILE2
               AT END DISPLAY MERCHANT-CONTACT(1) " = "
               WS-INPUT-RECORD1
               WHEN MERCHANT-CONTACT(WS-IDX) =
               WS-MERCHANT-CONTACT IN WS-INPUT-RECORD1
               ADD +1 TO WS-INDEX
               MOVE 100-MERCHANT-NUMBER TO WS-OUT-MERCHANT-NUMBER
               MOVE MERCHANT-ADDESS1(WS-IDX) TO WS-OUT-MERCHANT-ADDESS1
               MOVE MERCHANT-ADDESS2(WS-IDX) TO WS-OUT-MERCHANT-ADDESS2
               MOVE ZIP-CODE(WS-IDX)TO WS-OUT-ZIP-CODE
               ADD +1 TO WS-400-REC-COUNT
               MOVE WS-OUTPUT-RECORD4 TO 400-OUTPUT-RECORD
           END-SEARCH

           SEARCH WS-ARRAY-FILE3
               AT END DISPLAY MERCHANT-ACCOUNT(1) " = "
               WS-INPUT-RECORD1
               WHEN MERCHANT-ACCOUNT(WS-IDX) =
               WS-MERCHANT-ACCOUNT IN WS-INPUT-RECORD1
               MOVE MERCHANT-NUMBER(WS-IDX) TO WS-OUT-MERCHANT-ACCOUNT

               MOVE WS-OUTPUT-RECORD4 TO 400-OUTPUT-RECORD
           END-SEARCH

           PERFORM 7000-WRITE-FILE4 THRU 7000-EXIT
       .
       6000-EXIT.
           EXIT.
       7000-WRITE-FILE4.
           WRITE 400-OUTPUT-RECORD
           IF FILE-OK                      OF WS-400-FILE-STATUS
               CONTINUE
           ELSE
              DISPLAY "ABEND MESSAGE"
              PERFORM 9000-ABEND           THRU 9000-EXIT
           END-IF
       .
       7000-EXIT.
           EXIT.
       8000-READ-FILE1.
           READ 100-INPUT-FILE
           IF FILE-OK OF WS-100-FILE-STATUS
               ADD +1 TO WS-100-REC-COUNT
           ELSE
               IF FILE-AT-END OF WS-100-FILE-STATUS
                   CONTINUE
               ELSE
                   DISPLAY "ABEND MESSAGE"
                   PERFORM 9000-ABEND THRU 9000-EXIT
               END-IF
           END-IF
       .
       8000-EXIT.
           EXIT.
       8100-READ-FILE2.
           READ 200-INPUT-FILE
           IF FILE-OK OF WS-200-FILE-STATUS
               ADD +1 TO WS-200-REC-COUNT
           ELSE
               IF FILE-AT-END OF WS-200-FILE-STATUS
                   CONTINUE
               ELSE
                   DISPLAY "ABEND MESSAGE"
                   PERFORM 9000-ABEND THRU 9000-EXIT
               END-IF
           END-IF
           .
       8100-EXIT.
           EXIT.
       8200-READ-FILE3.
           READ 300-INPUT-FILE
           IF FILE-OK OF WS-300-FILE-STATUS
               ADD +1 TO WS-300-REC-COUNT
           ELSE
               IF FILE-AT-END OF WS-300-FILE-STATUS
                   CONTINUE
               ELSE
                   DISPLAY "ABEND MESSAGE"
                   PERFORM 9000-ABEND THRU 9000-EXIT
               END-IF
           END-IF
       .
       8200-EXIT.
           EXIT.
       9000-ABEND.
           MOVE 12 TO RETURN-CODE.
           GOBACK.
       9000-EXIT.
           EXIT.

       10000-CHECK-FILE.
           IF FILE-OK OF WS-100-FILE-STATUS THEN
              CONTINUE
           ELSE
              DISPLAY "ABEND MESSAGE"
              PERFORM 9000-ABEND           THRU 9000-EXIT
           END-IF
           IF FILE-OK OF WS-200-FILE-STATUS THEN
               CONTINUE
           ELSE
              DISPLAY "ABEND MESSAGE"
              PERFORM 9000-ABEND           THRU 9000-EXIT
           END-IF
           IF FILE-OK OF WS-300-FILE-STATUS THEN
              CONTINUE
           ELSE
              DISPLAY "ABEND MESSAGE"
              PERFORM 9000-ABEND           THRU 9000-EXIT
           END-IF
           IF FILE-OK OF WS-400-FILE-STATUS THEN
              CONTINUE
           ELSE
              DISPLAY "ABEND MESSAGE"
              PERFORM 9000-ABEND           THRU 9000-EXIT
           END-IF
           .
